<?php
include 'db_connection.php';


if(isset($_GET['user']))
{
    deleteVol();
}
else if(isset($_GET['oppid']))
{
    deleteOpp();
}

function deleteOpp()
{
    $opp = $_GET['oppid'];
    
    $conn = OpenCon();
    
    $sql = "DELETE FROM `Opp_Info` WHERE `Opp_Info`.`opp_id` = " . $opp;
    
    echo $sql;
    
    $result = mysqli_query($conn, $sql);
    
    header("Location: manage_opport.php");
}

function deleteVol()
{
    $username = $_GET['user'];
    $conn = OpenCon();
    
    $sql = "DELETE FROM `Vol_Basic` WHERE `Vol_Basic`.`username` ='" . $username . "'";
    
    $result = mysqli_query($conn, $sql);
    
    $sql = "DELETE FROM `Vol_Contact_Info` WHERE `Vol_Contact_Info`.`username` ='" . $username . "'";
    
    $result = mysqli_query($conn, $sql);
    
    $sql = "DELETE FROM `Vol_Education` WHERE `Vol_Education`.`username` ='" . $username . "'";
    
    $result = mysqli_query($conn, $sql);
    
    $sql = "DELETE FROM `Vol_Emergency_Contact` WHERE `Vol_Emergency_Contact`.`username` ='" . $username . "'";
    
    $result = mysqli_query($conn, $sql);
    
    $sql = "DELETE FROM `Vol_Placement_Info` WHERE `Vol_Placement_Info`.`username` ='" . $username . "'";
    
    $result = mysqli_query($conn, $sql);
    
    header("Location: manage_vol.php");
}