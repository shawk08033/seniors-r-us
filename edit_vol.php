<?php
session_start();
include 'db_connection.php';
include 'usercheck.php';

$username = $_POST['volUsername'];
$password = $_POST['volPassword'];
$firstname = $_POST['firstName'];
$lastname = $_POST['lastName'];
$cellPhone = $_POST['cellPhone'];
$homePhone = $_POST['homePhone'];
$workPhone = $_POST['workPhone'];
$email = $_POST['email'];
$street = $_POST['streetAdd'];
$city = $_POST['city'];
$state = $_POST['state'];
$zip = $_POST['zip'];
$dLicBool = $_POST['dLicBool'];
$ssBool = $_POST['ssBool'];
$approvalStatus = $_POST['approvalStatus'];
$schoolType = $_POST['schoolType'];
$schoolName = $_POST['schoolName'];
$schoolCity = $_POST['schoolCity'];
$schoolState = $_POST['schoolState'];
$gradYear = $_POST['gradYear'];
$ecName = $_POST['ecName'];
$ecHomePhone = $_POST['ecHomePhone'];
$ecCellPhone = $_POST['ecCellPhone'];
$ecWorkPhone = $_POST['ecWorkPhone'];
$ecEmail = $_POST['ecEmail'];
$ecStreetAdd = $_POST['ecStreetAdd'];
$ecCity = $_POST['ecCity'];
$ecState = $_POST['ecState'];
$ecZip = $_POST['ecZip'];

if(isset($_POST['volUsername'])) {
    $conn = OpenCon();
    $sql = "SELECT first_name, last_name FROM Vol_Basic WHERE username='$username'";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $fullName = $row["first_name"]." ".$row["last_name"];
            }
    }
    
    CloseCon($conn); 
    
}

if(isset($_POST['firstName'])) {
     editVolunteerBasic($username, $password, $firstname, $lastname,  $dLicBool, $ssBool, $approvalStatus);
}

if(isset($_POST['cellPhone'])) {
    editVolunteerContact($username, $cellPhone, $homePhone, $workPhone, $email, $street, 
        $city, $state, $zip);
}

if(isset($_POST['ecCellPhone'])) {
    editEmergencyContact($username, $ecName, $ecHomePhone, $ecCellPhone, $ecWorkPhone, $ecEmail, $ecStreetAdd, $ecCity, $ecState, $ecZip);
}

if(isset($_POST['schoolType'])) {
    editVolunteerEducation($username, $schoolType, $schoolName, $schoolCity, $schoolState, $gradYear);
}

function editVolunteerBasic($username, $password, $firstname, $lastname,  $dLicBool, $ssBool, $approvalStatus)
{
    $conn = OpenCon();
    $deleql = "DELETE FROM Vol_Basic WHERE username='$username'";
    if (mysqli_query($conn,$deleql)) {
        $sql= "INSERT INTO Vol_Basic (first_name, last_name, username, password, driverLicOnFile, socSecCardOnFile, approvalStatus)
            VALUES('$firstname','$lastname','$username','$password','$dLicBool', '$ssBool', '$approvalStatus')";
            if (mysqli_query($conn,$sql)) {
            echo "Basic Information record updated successfully";
            } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
    }    
    CloseCon($conn);
}









?>
<html>
    <head>
        <title>Edit Volunteer</title>
        <script src="script.js"></script>
        <link rel = "stylesheet" type = "text/css" href = "style.css" />
    </head>
<body>
    <nav>
    <ul>
        <li><a href = "search.php">Search Volunteers</a></li>
        <li><a href = "volinsert.php">Add Volunteer</a></li>
        <li><a href = "#">View Volunteer Opportunity Matches</a></li>
    </ul>
</nav>
<center><h2 id="header">Edit Volunteer Information</h2></center><br><br>
<form action="edit_vol.php" method= "post">
<table>
    <col style = "width:180">
    <tr>
        <td>Volunteer Username:</td>
        <td><input type = "text" name = "volUsername"></td>
        <td><button type="submit" style="background:#66CCFF; cursor:pointer">Go</button></td>
        <td>&nbsp&nbsp</td>
        <td><a href = "#">Find username</a></td>
    </tr>
    <tr></tr><tr></tr><tr></tr>
</table>
    <br><hr><br>
<table>
    <col style = "width:180">
    <tr>
        <td>Update Information for:</td>
        <td><input type = "text" style="background-color:#FCF5D8" 
        value="<?php echo @$fullName; ?>" name = "firstLastName" readonly></td>
    </tr>
</table>
</form>
 <br><hr><br>
 <form action = "edit_vol.php" method = "post"></form>
<table>
    <col style = "width:180">
       
    <tr>
        <td>First Name:</td>
        <td><input type = "text"name = "firstName"></td>
    </tr>
    <tr>
        <td>Last Name:</td>
        <td><input type = "text" name = "lastName"></td>
    </tr>
    <tr>
        <td>Password:</td>
        <td><input type = "text" name = "volPassword"></td>
    </tr>
    <tr>
        <td>Driver's License</td>
        <td><input type = "text" name = "dLicBool"></td>
    </tr>
    <tr>
        <td>Soc. Sec. Card</td>
        <td><input type = "text" name = "ssBool"></td>
    </tr>
    <tr>
        <td>Status:</td>
        <td><input type = "text" name = "approvalStatus"></td>
    </tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr><td></td><th><button style="width: 100%; background:#66CCFF; cursor:pointer"> <h3>Update Administrative</h3></button></th></tr>
    </table>
    <br><hr><br>
    <table>
    <col style = "width:180">
    <tr>
        <td>Cell Phone:</td>
        <td><input type = "text" name = "cellPhone"></td>
    </tr>
    <tr>
        <td>Home Phone:</td>
        <td><input type = "text" name = "homePhone"></td>
    </tr>
    <tr>
        <td>Work Phone:</td>
        <td><input type = "text" name = "workPhone"></td>
    </tr>
    <tr>
        <td>Email Address:</td>
        <td><input type = "text" name = "email"></td>
    </tr>
    <tr>
        <td>Street Address:</td>
        <td><input type = "text" name = "streetAdd"></td>
    </tr>
    <tr>
        <td>City:</td>
        <td><input type = "text" name = "city"></td>
    </tr>
    <tr>
        <td>State:</td>
        <td><input type = "text" name = "state"></td>
    </tr>
    <tr>
        <td>Zip:</td>
        <td><input type = "text" name = "zip"></td>
    </tr>
    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr><td></td><th><button style="width: 100%; background:#66CCFF; cursor:pointer"><h3>Update Contact</h3></button></th>
</table>
    <br><hr><br>
    <table>
    <col style = "width:180">
    <tr>
        <td>School Type:</td>
        <td><input type = "text" name = "schoolType"></td>
    </tr>
    <tr>
        <td>School Name:</td>
        <td><input type = "text" name = "schoolName"></td>
    </tr>
    <tr>
        <td>School City:</td>
        <td><input type = "text" name = "schoolCity"</td>
    </tr>
    <tr>
        <td>School State:</td>
        <td><input type = "text" name = "schoolState"></td>
    </tr>
    <tr>
        <td>Grad(or expected) Year:</td>
        <td><input type = "text" name = "gradYear"></td>
    </tr>
    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
        <tr><td><th><button style="width: 100%; background:#66CCFF; cursor:pointer"><h3>Update Education</h3></button></th><td>
        </table>
    <br><hr><br>
    <table>
    <col style = "width:180">
    <tr>
        <td>EC. Cell Phone:</td>
        <td><input type = "text" name = "ecCellPhone"></td>
    </tr>
    <tr>
        <td>EC. Home Phone:</td>
        <td><input type = "text" name = "ecHomePhone"></td>
    </tr>
    <tr>
        <td>EC. Work Phone:</td>
        <td><input type = "text" name = "ecWorkPhone"</td>
    </tr>
    <tr>
        <td>EC. Email Address:</td>
        <td><input type = "text" name = "ecEmail"></td>
    </tr>
    <tr>
        <td>EC. Street Address:</td>
        <td><input type = "text" name = "ecStreetAdd"></td>
    </tr>
    <tr>
        <td>EC. City:</td>
        <td><input type = "text" name = "ecCity"></td>
    </tr>
    <tr>
        <td>EC. State:</td>
        <td><input type = "text" name = "ecState"></td>
    </tr>
    <tr>
        <td>EC. Zip:</td>
        <td><input type = "text" name = "ecZip"></td>
    </tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr><td></td><th><button style="width: 100%; background:#66CCFF; cursor:pointer"><h3>Update Emer. Contact</h3></button></th>
    </tr>
</table>
</form>
    
   
</body>
</html>


