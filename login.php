<?php
include 'db_connection.php';

$conn = OpenCon();

$sql = "SELECT admin_username, admin_password FROM Administrator_Info";
$result = mysqli_query($conn, $sql);

if(isset($_POST))
{
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
       if ($_POST['username'] == $row["admin_username"] && $_POST['password'] == $row["admin_password"])
	   {
			session_start();
			$_SESSION['username'] = $_POST['username'];
			header('Location: index.php');
	   }
    }
} else {
    echo "worng username and password";
}
}

CloseCon($conn);
?>
<html>
<head><title>Admin Login</title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
<head>
<center>
<body>
<h1 id = "header">Admin Login test</h1>
<div id = "form">
<form action = "login.php" method = "post">
<table>
<tr>
<td>Username:</td>
<td><input type = "text" name = "username" required></td>
</tr>
<tr>
<td>Password:</td>
<td><input type = "password" name = "password" required></td>
</tr>
<tr>
<br>
<td><input type = "submit" value = "Login">
<input type = "reset" value = "Clear"></td>
</tr>
</div>
</table>
</center>
</form>

<div id = "footer">
Copyright &copy; 2019 Seniors'R'Us
</div>

</body>

</html>