<html>
    <head><title>Edit Opportunity</title>
        <link rel = "stylesheet" type = "text/css" href = "style.css" />
    </head>
    <body>
	<?php include 'usercheck.php';?>
        <h1 id="header">Edit Opportunities</h1>
    </body>
</html>
<?php

include 'db_connection.php';

    $conn = OpenCon();
    if ($conn->connection_error){
        die("Connection failed: " . $conn->connect_error);
        echo "connection failed";
    }
    $sql = "SELECT * FROM Opp_Info";
    $results = $conn->query($sql);
    
if ($results->num_rows > 0) {
    // output data of each row
    echo"<table border = '1'>";
    echo"<tr><th>ID</th><th>Type</th><th>Date</th><th>Location</th><th>Center</th><br></tr>";
    while($row = $results->fetch_assoc()) {
        "<tr>
            <td>{$row["opp_id"]}</td>
            <td>{$row["opp_type"]}</td>
            <td>{$row["opp_date"]}</td>
            <td>{$row["opp_location"]}</td>
            <td>{$row["opp_center"]}</td>
            <td><a href=\"edit_opp.php?edit={$row["opp_id"]}\">Edit</a></td>
            </tr>";
    }
    echo"</tables>";
} else {
    echo "0 results";
}
$conn->close();
?>