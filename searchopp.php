<?php
error_reporting(-1);
ini_set('display_errors', 'On');
session_start();

include 'db_connection.php';
include 'usercheck.php';
?>


<!DOCTYPE html>

<head>
<title>Search Opportunities</title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>


<body>
<center>
<h3 id = "header">Search for existing opportunities</h3></center>
<div id = "formsearch">
<form action = "searchopp.php" method = "post">
<input type = "text" name = "search" placeholder = "Search for opportunities...">
<input type = "submit" value = ">>">

</form>
<br>
<table>
    <tr><th>Opportunity Type</th><th>Opportunity Date</th><th>Opportunity Location</th><th>Opportunity Center</th></tr>
    <br>
<?
    if(isset($_POST['search']))
{
    $conn = OpenCon();
   
	$searchq = $_POST['search'];
	$searchq = preg_replace("#[^0-9a-z]#i","", $searchq);

	
	$sql = "SELECT * FROM Opp_Info WHERE opp_type LIKE '$searchq' OR opp_location LIKE '$searchq'";
		
	if(trim($_POST['search']) == '')
	{
	    $sql = "SELECT * FROM Opp_Info";
	}
	
	$result = mysqli_query($conn, $sql);
	$count = mysqli_num_rows($result);
	if($count == 0)
	{
	$output = 'No results!';
	}
	else
	{
	while($row = mysqli_fetch_assoc($result))
	{
	$oppType = $row['opp_type'];
	$oppDate = $row['opp_date'];
	$oppLoc = $row['opp_location'];
	$oppCent = $row['opp_center'];
	$id = $row['opp_id'];
	
	$output = '<tr><td> '.$oppType.'</td><td> '.$oppDate.'</td><td> '.$oppLoc.'</td><td> '.$oppCent.'</td><td><a href="delete.php?oppid=' . $id . '">delete</a></td><td><a href="edit_opp.php?edit='. $id . '">edit</a></td></tr>';
	echo "$output";
	}
	}
}?>
</table>
</div>

<div id = "footer">
Copyright &copy; 2019 Seniors'R'Us
</div>
</body>

</html>