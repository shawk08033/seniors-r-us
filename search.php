<?php
error_reporting(-1);
ini_set('display_errors', 'On');

session_start();
include 'db_connection.php';
include 'usercheck.php';
?>

<!DOCTYPE html>
<html>
<head>
<title>Search</title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
   $('#filter').on('change', function () {
     var selectVal = $("#filter option:selected").val();
     if(selectVal == "Driver License")
     {
         $('#loc').hide();
         $('edu').hide();
         $('#drlic').show();
     }
     else if(selectVal == "Eduction")
     {
        $('#drlic').hide();
         $('#loc').hide();
         $('#edu').show();
     }
     else if(selectVal == "Location")
     {
         $('#drlic').hide();
         $('#edu').hide();
         $('#loc').show();
     }
     else if(selectVal == "All")
     {
         $('#drlic').hide();
         $('#loc').hide();
         $('#edu').hide();
     }
});
});
</script>
</head>
<body>


<center>
<h3 id = "header">Search for existing volunteers here</h3></center>
<nav>
<ul>
<li><a href = "manage_vol.php">Home</a></li>
<li><a href = "volinsert.php">Add volunteer</a></li>
<li><a href = "#">Edit Volunteer</a></li>
<li><a href = "#">View Volunteer Opportunity Matches</a></li>
</ul>
</nav>
<div id = "formsearch">
<form action = "search.php" method = "post">
<input type = "text" name = "search" placeholder = "Search for volunteers...">
<select id="filter" name="filter">
    <option value="All">All</option>
    <option value="Driver License">Driver License</option>
    <option value="Eduction">Center</option>
    <option value="Location">Location</option>
</select>
<div style="border-style: solid;">
    <input type="checkbox" name="approved" value="1"> Approved &nbsp; <input type="checkbox" name="pending" value="1"> Pending &nbsp; <input type="checkbox" name="denied" value="1"> Denied &nbsp; <input type="checkbox" name="Inactive" value="1"> Inactive
</div>
<!--driver  license -->
<div id="drlic" style="display:none">
<input type="radio" name="license" value="yes"> Yes &nbsp; <input type="radio" name="license" value="no"> no
</div>
<!---->
<!--Location-->
<div id="loc" style="display:none">
<input type="checkbox" name="north" value="north"> North &nbsp; <input type="checkbox" name="south" value="south"> South &nbsp; <input type="checkbox" name="east" value="east"> East &nbsp; <input type="checkbox" name="west" value="west"> West
</div>
<!---->
<!--Edu-->
<div id="edu" style="display:none">
    
</div>
<!---->
<input type = "submit" value = ">>">
</form>
<table>
    <tr><th>First Name</th><th>Last Name</th><th>Username</th><th>Password</th></tr>
   <?
    if(isset($_POST['search']))
{
    $conn = OpenCon();
   
	$searchq = $_POST['search'];
	$searchq = preg_replace("#[^0-9a-z]#i","", $searchq);
	
	$sql = "SELECT * FROM Vol_Basic WHERE last_name LIKE '$searchq' OR first_name LIKE '$searchq'";
	
	if(trim($_POST['search']) == '')
	{
	    $sql = "SELECT * FROM Vol_Basic";
	}
	
	if($_POST['approved'] == "1")
	{
    	if(trim($_POST['search']) == '')
    	{
    	    $sql .= " WHERE `approvalStatus` LIKE 'approved' ORDER BY `approvalStatus`";
    	}
    	else
    	{
	        $sql .= $sql = " AND `approvalStatus` LIKE 'approved' ORDER BY `approvalStatus`";
    	}
	}
	else if($_POST['pending'] == "1")
	{
	    if(trim($_POST['search']) == '')
    	{
    	    $sql .= " WHERE `approvalStatus` LIKE 'pending' ORDER BY `approvalStatus`";
    	}
    	else
    	{
	        $sql .= " AND `approvalStatus` LIKE 'pending' ORDER BY `approvalStatus`";
    	}
	}
	else if($_POST['denied'] == "1")
	{
	    if(trim($_POST['search']) == '')
    	{
    	    $sql .= " WHERE `approvalStatus` = 'denied' ORDER BY `approvalStatus`";
    	}
    	else
    	{
	        $sql .= " AND `approvalStatus` LIKE 'denied' ORDER BY `approvalStatus`";
    	}
	}
	else if($_POST['inactive'] == "1")
	{
	    if(trim($_POST['search']) == '')
    	{
    	    $sql .= " WHERE `approvalStatus` = 'inactive' ORDER BY `approvalStatus`";
    	}
    	else
    	{
	        $sql .= " AND `approvalStatus` LIKE 'inactive' ORDER BY `approvalStatus`";
    	}
	}
	
	$result = mysqli_query($conn, $sql);
	$count = mysqli_num_rows($result);
	if($count == 0)
	{
	$output = 'No results!';
	}
	else
	{
	while($row = mysqli_fetch_assoc($result))
	{
	$fname = $row['first_name'];
	$lname = $row['last_name'];
	$username = $row['username'];
	$password = $row['password'];
	
	$output = '<tr><td> '.$fname.'</td><td> '.$lname.'</td><td> '.$username.'</td><td> '.$password.'</td><td><a href="delete.php?user=' . $username . '">delete</a></td></tr>';
	echo "$output";
	}
	}
}
?>
</table>
</div>
<div id = "footer">
Copyright &copy; 2019 Seniors'R'Us
</div>

</body>

</html>