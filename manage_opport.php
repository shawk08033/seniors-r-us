<?php 
session_start();
include 'db_connection.php';
$conn = OpenCon();
include 'usercheck.php';
?>

<!DOCTYPE html>

<head><title>List of Filters</title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>

<body>
<center><h2 id = "header">Manage Volunteers</h2></center>
<nav>
<ul>
<li><a href = "searchopp.php">Search Opportunity</a></li>
<li><a href = "add_opp.php">Add New Opportunity</a></li>
<li><a href = "#">View Volunteer Opportunity Matches</a></li>
</ul>
</nav>
<center>
    <h2>This Week</h2>
<table>
    <tr><th>Opportunity Type</th><th>Opportunity Date</th><th>Opportunity Location</th><th>Opportunity Center</th></tr>
    <br>
<?

    $conn = OpenCon();
   
	$searchq = $_POST['search'];
	$searchq = preg_replace("#[^0-9a-z]#i","", $searchq);

	
	$sql = "SELECT * FROM Opp_Info WHERE WEEKOFYEAR(opp_date) = WEEKOFYEAR(NOW())ORDER BY opp_date DESC;";
	
	
	$result = mysqli_query($conn, $sql);
	$count = mysqli_num_rows($result);
	if($count == 0)
	{
	$output = 'No results!';
	}
	else
	{
	while($row = mysqli_fetch_assoc($result))
	{
	$oppType = $row['opp_type'];
	$oppDate = $row['opp_date'];
	$oppLoc = $row['opp_location'];
	$oppCent = $row['opp_center'];
	$id = $row['opp_id'];
	
	$output = '<tr><td> '.$oppType.'</td><td> '.$oppDate.'</td><td> '.$oppLoc.'</td><td> '.$oppCent.'</td><td><a href="delete.php?oppid=' . $id . '">delete</a></td><td><a href="edit_opp.php?edit='. $id . '">edit</a></td></tr>';
	echo "$output";
	}
	}
	?>
</table>
</center>
<div id = "footer">
Copyright &copy; 2019 Seniors'R'Us
</div>
</body>

</html>