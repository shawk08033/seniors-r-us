<?php 
session_start();
include 'db_connection.php';

include 'usercheck.php';

?>

<html>
<head><title>Manage Volunteers</title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>

<body>
<center><h2 id = "header">Manage Volunteers</h2></center>
<nav>
<ul>
<li><a href = "search.php">Search Volunteers</a></li>
<li><a href = "volinsert.php">Add volunteer</a></li>
<li><a href = "edit_vol.php">Edit Volunteer</a></li>
<li><a href = "#">View Volunteer Opportunity Matches</a></li>
</ul>
</nav>
<center>
<h1>Pending</h1>
<table>
    <tr><th>First Name</th><th>Last Name</th><th>Username</th><th>Password</th></tr>
   <?php
    $conn = OpenCon();
	
	$sql = "SELECT * FROM `Vol_Basic` WHERE `approvalStatus` = 'pending'";
	$result = mysqli_query($conn, $sql);
	$count = mysqli_num_rows($result);

	while($row = mysqli_fetch_assoc($result))
	{
	$fname = $row['first_name'];
	$lname = $row['last_name'];
	$username = $row['username'];
	$password = $row['password'];
	
	$output = '<tr><td> '.$fname.'</td><td> '.$lname.'</td><td> '.$username.'</td><td> '.$password.'</td><td><a href="approvedeny.php?status=app&user='. $username . '">Approve</a></td><td><a href="approvedeny.php?status=deny&user='. $username . '">Deny</a></tr>';
	echo "$output";
	}
?>
</table>
</center>
<div id = "footer">
Copyright &copy; 2019 Seniors'R'Us
</div>
</body>

</html>