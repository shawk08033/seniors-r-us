<?php
error_reporting(-1);
ini_set('display_errors', 'On');

session_start();
include 'db_connection.php';

include 'usercheck.php';
?>

<html>
<head><title>Add Volunteer</title>
 <script src="script.js"></script>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>
<body>
<h1 id= "header">Add New Volunteer</h1>

<nav>
<ul>
<li><a href = "manage_vol.php">Home</a></li>
<li><a href = "search.php">Search Volunteer</a></li>
<li><a href = "#">Edit Volunteer</a></li>
<li><a href = "#">View Volunteer Opportunity Matches</a></li>
</ul>
</nav>

<br>
<h4>Login Information</h4><br>
<form action = "volinsert.php" method = "post">
<table>
<col style="width:180" >
    <tr>
        <td>First Name:</td>
        <td><input type = "text" name = "firstName"></td>
    </tr>
    <tr>
        <td>Last Name:</td>
        <td><input type = "text" name = "lastName"></td>
    </tr>
        <td>Username:</td>
        <td><input type = "text" name = "volUsername"></td>
    </tr>
        <td>Password:</td>
        <td><input type = "text" name = "volPassword"></td>
    </tr>
    
</table>
<br>
<hr><br>
<h4>Contact Information</h4><br>
<table>
    <script src="script.js"></script>
<col style="width:180">
    <tr>
        <td>Cell Phone Number:</td>
        <td><input type = "text" name = "cellPhone" id = "cellPhone"></td>
    </tr>
    <tr>
        <td>Home Phone Number:</td>
        <td><input type = "text" name = "homePhone" id = "homePhone"></td>
    </tr>
        <td>Work Number:</td>
        <td><input type = "text" name = "workPhone" id = "workPhone"></td>
    </tr>
        <td>Email Address:</td>
        <td><input type = "text" name = "email"></td>
    </tr>
    <tr>
        <td>Street Address:</td>
        <td><input type = "text" name = "streetAdd"></td>
    </tr>
    <tr>
        <td>City:</td>
        <td><input type = "text" name = "city"></td>
    </tr>
    <tr></tr>
        <td>State:</td>
        <td><select name = "state" style="width:173">
            <option value="select">Select State</option>
           	<option value="AL">Alabama</option>
		    <option value="AK">Alaska</option>
		    <option value="AZ">Arizona</option>
		    <option value="AR">Arkansas</option>
		    <option value="CA">California</option>
		    <option value="CO">Colorado</option>
		    <option value="CT">Connecticut</option>
		    <option value="DE">Delaware</option>
		    <option value="DC">District Of Columbia</option>
		    <option value="FL">Florida</option>
		    <option value="GA">Georgia</option>
		    <option value="HI">Hawaii</option>
		    <option value="ID">Idaho</option>
		    <option value="IL">Illinois</option>
		    <option value="IN">Indiana</option>
		    <option value="IA">Iowa</option>
		    <option value="KS">Kansas</option>
		    <option value="KY">Kentucky</option>
		    <option value="LA">Louisiana</option>
		    <option value="ME">Maine</option>
		    <option value="MD">Maryland</option>
		    <option value="MA">Massachusetts</option>
		    <option value="MI">Michigan</option>
		    <option value="MN">Minnesota</option>
		    <option value="MS">Mississippi</option>
		    <option value="MO">Missouri</option>
		    <option value="MT">Montana</option>
		    <option value="NE">Nebraska</option>
		    <option value="NV">Nevada</option>
		    <option value="NH">New Hampshire</option>
		    <option value="NJ">New Jersey</option>
		    <option value="NM">New Mexico</option>
		    <option value="NY">New York</option>
		    <option value="NC">North Carolina</option>
		    <option value="ND">North Dakota</option>
		    <option value="OH">Ohio</option>
		    <option value="OK">Oklahoma</option>
		    <option value="OR">Oregon</option>
		    <option value="PA">Pennsylvania</option>
		    <option value="RI">Rhode Island</option>
		    <option value="SC">South Carolina</option>
		    <option value="SD">South Dakota</option>
		    <option value="TN">Tennessee</option>
		    <option value="TX">Texas</option>
		    <option value="UT">Utah</option>
		    <option value="VT">Vermont</option>
		    <option value="VA">Virginia</option>
		    <option value="WA">Washington</option>
		    <option value="WV">West Virginia</option>
		    <option value="WI">Wisconsin</option>
		    <option value="WY">Wyoming</option>
		    </select>
            </td>
    </tr>
    <tr>
        <td>Zip:</td>
        <td><input type = "text" name = "zip"></td>
    </tr>
    
</table>
<br><hr><br>
<h4>Administrative Information</h4><br>
<table>
<col style="width:180">
    <tr>
        <td>Drivers License on File:</td>
        <td><input type = "radio" name = "dLicBool" value = "yes">Yes&nbsp&nbsp   
            <input type = "radio" name = "dLicBool" value = "no">No</td>
    </tr>
    <tr>
        <td>Soc. Sec. Card on File:</td>
        <td><input type = "radio" name = "ssBool" value = "yes">Yes&nbsp&nbsp&nbsp<input type = "radio" name = "ssBool" value = "no">No</td>
    </tr>
        <td>Approval Status:</td>
        <td><input type = "radio" name = "approvalStatus" value = "approved">Approved&nbsp&nbsp
            <input type = "radio" name = "approvalStatus" value = "pending">Pending&nbsp&nbsp
            <input type = "radio" name = "approvalStatus" value = "denied">Denied</td>
    </tr>
        
</table>
<br><hr><br>
<h4>Education Information</h4><br>
<table>
<col style="width:180">
    <tr>
        <td>Type of institution attended:</td>
        <td><select name = "schoolType" style="width:173">
                <option value = "highSchool">High School</option>
                <option value = "tradeSchool">Trade School</option>
                <option value = "techSchool">Technical School</option>
                <option value = "college">College</option>
                <option value = "university">University</option>
                <option value = "postGrad">Post-Graduate</option>
        </select></td>
    </tr>
    
    <tr>
        <td>Name of Institution:</td>
        <td><input type = "text" name = "schoolName"></td>
    </tr>
    <tr>
        <td>City:</td>
        <td><input type = "text" name = "schoolCity"></td>
    </tr>
    <tr>
        <td>State:</td>
        <td><select name = "schoolState" style="width:173">
            <option value="select">Select State</option>
           	<option value="AL">Alabama</option>
		    <option value="AK">Alaska</option>
		    <option value="AZ">Arizona</option>
		    <option value="AR">Arkansas</option>
		    <option value="CA">California</option>
		    <option value="CO">Colorado</option>
		    <option value="CT">Connecticut</option>
		    <option value="DE">Delaware</option>
		    <option value="DC">District Of Columbia</option>
		    <option value="FL">Florida</option>
		    <option value="GA">Georgia</option>
		    <option value="HI">Hawaii</option>
		    <option value="ID">Idaho</option>
		    <option value="IL">Illinois</option>
		    <option value="IN">Indiana</option>
		    <option value="IA">Iowa</option>
		    <option value="KS">Kansas</option>
		    <option value="KY">Kentucky</option>
		    <option value="LA">Louisiana</option>
		    <option value="ME">Maine</option>
		    <option value="MD">Maryland</option>
		    <option value="MA">Massachusetts</option>
		    <option value="MI">Michigan</option>
		    <option value="MN">Minnesota</option>
		    <option value="MS">Mississippi</option>
		    <option value="MO">Missouri</option>
		    <option value="MT">Montana</option>
		    <option value="NE">Nebraska</option>
		    <option value="NV">Nevada</option>
		    <option value="NH">New Hampshire</option>
		    <option value="NJ">New Jersey</option>
		    <option value="NM">New Mexico</option>
		    <option value="NY">New York</option>
		    <option value="NC">North Carolina</option>
		    <option value="ND">North Dakota</option>
		    <option value="OH">Ohio</option>
		    <option value="OK">Oklahoma</option>
		    <option value="OR">Oregon</option>
		    <option value="PA">Pennsylvania</option>
		    <option value="RI">Rhode Island</option>
		    <option value="SC">South Carolina</option>
		    <option value="SD">South Dakota</option>
		    <option value="TN">Tennessee</option>
		    <option value="TX">Texas</option>
		    <option value="UT">Utah</option>
		    <option value="VT">Vermont</option>
		    <option value="VA">Virginia</option>
		    <option value="WA">Washington</option>
		    <option value="WV">West Virginia</option>
		    <option value="WI">Wisconsin</option>
		    <option value="WY">Wyoming</option>
		    </select>
            </td>
    </tr>
    <tr>
        <td>Graduation (or expected) year:</td>
        <td><input type = "text" name = "gradYear"></td>
    </tr>
    
        
</table>
<br><hr><br>
<h4>Emergency Contact Information</h4><br>
<table>
<col style="width:180">
    <tr>
        <td>Contact Name:</td>
        <td><input type = "text" name = "ecName"></td>
    </tr>
    <tr>
        <td>Home Phone No:</td>
        <td><input type = "text" name = "ecHomePhone" id = "ecHomePhone"></td>
    </tr>
        <td>Cell Phone No:</td>
        <td><input type = "text" name = "ecCellPhone" id = "ecCellPhone"></td>
    </tr>
        <td>Work Phone No:</td>
        <td><input type = "text" name = "ecWorkPhone" id = "ecWorkPhone"></td>
    </tr>
    <tr>
        <td>Email:</td>
        <td><input type = "text" name = "ecEmail"></td>
    </tr>
    <tr>
        <td>Street Address:</td>
        <td><input type = "text" name = "ecStreetAdd"></td>
    </tr>
    <tr>
        <td>City:</td>
        <td><input type = "text" name = "ecCity"></td>
    </tr>
    <tr>
        <td>State:</td>
        <td><select name = "ecState" style="width:173">
            <option value="select">Select State</option>
           	<option value="AL">Alabama</option>
		    <option value="AK">Alaska</option>
		    <option value="AZ">Arizona</option>
		    <option value="AR">Arkansas</option>
		    <option value="CA">California</option>
		    <option value="CO">Colorado</option>
		    <option value="CT">Connecticut</option>
		    <option value="DE">Delaware</option>
		    <option value="DC">District Of Columbia</option>
		    <option value="FL">Florida</option>
		    <option value="GA">Georgia</option>
		    <option value="HI">Hawaii</option>
		    <option value="ID">Idaho</option>
		    <option value="IL">Illinois</option>
		    <option value="IN">Indiana</option>
		    <option value="IA">Iowa</option>
		    <option value="KS">Kansas</option>
		    <option value="KY">Kentucky</option>
		    <option value="LA">Louisiana</option>
		    <option value="ME">Maine</option>
		    <option value="MD">Maryland</option>
		    <option value="MA">Massachusetts</option>
		    <option value="MI">Michigan</option>
		    <option value="MN">Minnesota</option>
		    <option value="MS">Mississippi</option>
		    <option value="MO">Missouri</option>
		    <option value="MT">Montana</option>
		    <option value="NE">Nebraska</option>
		    <option value="NV">Nevada</option>
		    <option value="NH">New Hampshire</option>
		    <option value="NJ">New Jersey</option>
		    <option value="NM">New Mexico</option>
		    <option value="NY">New York</option>
		    <option value="NC">North Carolina</option>
		    <option value="ND">North Dakota</option>
		    <option value="OH">Ohio</option>
		    <option value="OK">Oklahoma</option>
		    <option value="OR">Oregon</option>
		    <option value="PA">Pennsylvania</option>
		    <option value="RI">Rhode Island</option>
		    <option value="SC">South Carolina</option>
		    <option value="SD">South Dakota</option>
		    <option value="TN">Tennessee</option>
		    <option value="TX">Texas</option>
		    <option value="UT">Utah</option>
		    <option value="VT">Vermont</option>
		    <option value="VA">Virginia</option>
		    <option value="WA">Washington</option>
		    <option value="WV">West Virginia</option>
		    <option value="WI">Wisconsin</option>
		    <option value="WY">Wyoming</option>
		    </select>
            </td>
    </tr>
    <tr>
        <td>Zip:</td>
        <td><input type = "text" name = "ecZip"></td>
    </tr>
    
</table>
<br><hr><br>
<h4>Placement Information</h4><br>
<table>
<col style="width:180">
    <tr>
        <td>Preferred Center #1:</td>
        <td><input type = "radio" name = "center1" value = "east">East&nbsp&nbsp
        <input type = "radio" name = "center1" value = "west">West&nbsp&nbsp
        <input type = "radio" name = "center1" value = "north">North&nbsp&nbsp
        <input type = "radio" name = "center1" value = "south">South&nbsp&nbsp</td>
    </tr>
    <tr>
        <td>Preferred Center #2:</td>
        <td><input type = "radio" name = "center2" value = "east">East&nbsp&nbsp
        <input type = "radio" name = "center2" value = "west">West&nbsp&nbsp
        <input type = "radio" name = "center2" value = "north">North&nbsp&nbsp
        <input type = "radio" name = "center2" value = "south">South&nbsp&nbsp</td>
    </tr>
    </table>
    <br><hr><br>
    <table>
    <col style="width:180">
    <tr>
        <td>Skills/Interests:</td>
        <td><input type = "checkbox" name = "skills[]" value = "crafts">Crafts&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "dancing">Dancing&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "gardening">Gardening&nbsp&nbsp</td></tr>
            <tr>
            <td></td>
            <td>
            <input type = "checkbox" name = "skills[]" value = "golfing">Golfing&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "swimming">Swimming&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "cards">Card Games&nbsp&nbsp</td></tr>
            <tr>
            <td></td>
            <td>
            <input type = "checkbox" name = "skills[]" value = "reading">Reading&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "walking">Walking&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <input type = "checkbox" name = "skills[]" value = "cooking">Cooking&nbsp&nbsp</td>
    </tr>
        
    
</table>
<br><hr><br>
<table>
<col style="width:180">
<tr>
    <td>Days Available:</td>
    <td>Sunday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "sunday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "sunday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "sunday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Monday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "monday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "monday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "monday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Tuesday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "tuesday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "tuesday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "tuesday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Wednesday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "wednesday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "wednesday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "wednesday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Thursday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "thursday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "thursday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "thursday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Friday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "friday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "friday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "friday" value = "fullDay">full day</td>
</tr>
<tr>
<td></td>    
<td>Saturday</td>
    <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
    <td><input type = "radio" name = "saturday" value = "morning">mornings&nbsp&nbsp</td>
    <td><input type = "radio" name = "saturday" value = "evening">evenings&nbsp&nbsp</td>
    <td><input type = "radio" name = "saturday" value = "fullDay">full day</td>
</tr>
</table>
<br><hr><br>
<center><input type = "submit" value = "Submit" >
<input type = "reset" value = "Clear"></td></center>
<script src= "script.js"></script>
</form>

<?php
//include 'db_connection.php';

$username = $_POST['volUsername'];
$password = $_POST['volPassword'];
$firstname = $_POST['firstName'];
$lastname = $_POST['lastName'];
$cellPhone = $_POST['cellPhone'];
$homePhone = $_POST['homePhone'];
$workPhone = $_POST['workPhone'];
$email = $_POST['email'];
$street = $_POST['streetAdd'];
$city = $_POST['city'];
$state = $_POST['state'];
$zip = $_POST['zip'];
$dLicBool = $_POST['dLicBool'];
$ssBool = $_POST['ssBool'];
$approvalStatus = $_POST['approvalStatus'];
$schoolType = $_POST['schoolType'];
$schoolName = $_POST['schoolName'];
$schoolCity = $_POST['schoolCity'];
$schoolState = $_POST['schoolState'];
$gradYear = $_POST['gradYear'];
$ecName = $_POST['ecName'];
$ecHomePhone = $_POST['ecHomePhone'];
$ecCellPhone = $_POST['ecCellPhone'];
$ecWorkPhone = $_POST['ecWorkPhone'];
$ecEmail = $_POST['ecEmail'];
$ecStreetAdd = $_POST['ecStreetAdd'];
$ecCity = $_POST['ecCity'];
$ecState = $_POST['ecState'];
$ecZip = $_POST['ecZip'];
$center1 = $_POST['center1'];
$center2 = $_POST['center2'];
$skills = implode(', ', $_POST['skills']);
$sunday = $_POST['sunday'];
$monday = $_POST['monday'];
$tuesday = $_POST['tuesday'];
$wednesday = $_POST['wednesday'];
$thursday = $_POST['thursday'];
$friday = $_POST['friday'];
$saturday = $_POST['saturday'];

if(isset($_POST['volUsername'])) {
    
    $conn = OpenCon();
    $sql = "SELECT * FROM Vol_Basic WHERE username='$username'";
    $res = mysqli_query($conn, $sql);
    
    if (mysqli_num_rows($res) > 0) {
  	  echo "Sorry... username already taken"; 	
  	}
  	else{
        addVolunteerBasic($username, $password, $firstname, $lastname,  $dLicBool, $ssBool, $approvalStatus);
        addVolunteerContact($username, $cellPhone, $homePhone, $workPhone, $email, $street, 
            $city, $state, $zip);
        addVolunteerEducation($username, $schoolType, $schoolName, $schoolCity, $schoolState, $gradYear);
        addEmergencyContact($username, $ecName, $ecHomePhone, $ecCellPhone, $ecWorkPhone, $ecEmail, $ecStreetAdd, $ecCity, $ecState, $ecZip);
        addPlacementInfo($username, $center1, $center2, $skills, $sunday, $monday, 
            $tuesday, $wednesday, $thursday, $friday, $saturday);
  	}
    
}

function addVolunteerBasic($username, $password, $firstname, $lastname,  $dLicBool, $ssBool, $approvalStatus)
{
    $conn = OpenCon();
   $sql = "INSERT INTO Vol_Basic (first_name, last_name, username, password,
        driverLicOnFile, socSecCardOnFile, approvalStatus)
VALUES ('$firstname','$lastname', '$username', '$password', '$dLicBool', '$ssBool', '$approvalStatus')";

if (mysqli_query($conn,$sql)) {
    echo "Basic Information record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
    CloseCon($conn);
}

function addVolunteerContact($username, $cellPhone, $homePhone, $workPhone, $email,
$street, $city, $state, $zip) {
    $conn = OpenCon();
    $sql = "INSERT INTO Vol_Contact_Info (username, cell_number, home_number, work_number, email_address, street_address, city, state, zip) VALUES('$username', '$cellPhone', '$homePhone', '$workPhone', '$email', '$street', 
        '$city', '$state', '$zip')";

    if (mysqli_query($conn,$sql)) {
    echo "Contact record created successfully";
    } 
    else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }
    CloseCon($conn);
}

function addVolunteerEducation($username, $schoolType, $schoolName, $schoolCity, $schoolState, $gradYear) {
    $conn = OpenCon();
    $sql = "INSERT INTO Vol_Education (username, school_type, school_name, school_city, school_state, gradOrExpected_year) VALUES('$username', '$schoolType', '$schoolName', '$schoolCity', '$schoolState', '$gradYear')";

    if (mysqli_query($conn,$sql)) {
    echo "Education record created successfully";
    } 
    else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }
    CloseCon($conn);
}

function addEmergencyContact($username, $ecName, $ecHomePhone, $ecCellPhone, $ecWorkPhone, $ecEmail, $ecStreetAdd, $ecCity, $ecState, $ecZip) {
    $conn = OpenCon();
    $sql = "INSERT INTO Vol_Emergency_Contact (username, contact_name, contact_homePhone, contact_cellPhone, contact_workPhone, contact_email, contact_streetAdd, contact_city, contact_state, contact_zip) VALUES('$username', '$ecName', '$ecHomePhone', '$ecCellPhone', '$ecWorkPhone', '$ecEmail', '$ecStreetAdd', '$ecCity', '$ecState', '$ecZip')";

    if (mysqli_query($conn,$sql)) {
    echo "Emergency contact record created successfully";
    } 
    else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    }
    CloseCon($conn);
}

function addPlacementInfo($username, $center1, $center2, $skills, $sunday, $monday, 
        $tuesday, $wednesday, $thursday, $friday, $saturday) {
    $conn = OpenCon();
    $sql = "INSERT INTO Vol_Placement_Info (username, preferredCenter_1, 
                preferredCenter_2, skills_interests, sunday, monday, tuesday,
                wednesday, thursday, friday, saturday) VALUES ('$username',
                '$center1', '$center2', '$skills', '$sunday', '$monday', 
                '$tuesday', '$wednesday', '$thursday', '$friday', '$saturday')";
    if(mysqli_query($conn,$sql)) {
    echo "Placement record created successfully";
    }
    else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    CloseCon($conn);
}
?>
</body>
</html>
